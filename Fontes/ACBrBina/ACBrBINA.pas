{******************************************************************************}
{ Projeto: Componentes ACBr                                                    }
{  Biblioteca multiplataforma de componentes Delphi para intera��o com equipa- }
{ mentos de Automa��o Comercial utilizados no Brasil                           }
{                                                                              }
{ Direitos Autorais Reservados (c) 2004 Daniel Simoes de Almeida               }
{                                                                              }
{ Colaboradores nesse arquivo:                                                 }
{                                                                              }
{  Voc� pode obter a �ltima vers�o desse arquivo na pagina do  Projeto ACBr    }
{ Componentes localizado em      http://www.sourceforge.net/projects/acbr      }
{                                                                              }
{ Esse arquivo usa a classe  SynaSer   Copyright (c)2001-2003, Lukas Gebauer   }
{  Project : Ararat Synapse     (Found at URL: http://www.ararat.cz/synapse/)  }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{                                                                              }
{ Daniel Sim�es de Almeida  -  daniel@djsystem.com.br  -  www.djsystem.com.br  }
{              Pra�a Anita Costa, 34 - Tatu� - SP - 18270-410                  }
{                                                                              }
{******************************************************************************}

{******************************************************************************
|* Historico
|*
|* 29/02/2016: Marcos R. Weimer
|*  - Primeira Versao ACBrBINA
******************************************************************************}

{$I ACBr.inc}

unit ACBrBINA;

interface

uses
  Classes, SysUtils, ExtCtrls,
  ACBrBase, ACBrDevice, ACBrBINAInterface;

const
   CACBrBINA_Versao = '0.0.1' ;

type
  TACBrBINAModelo = (binaNenhum, binaTopTronDD2);

  TACBrBINALeSerial = procedure(ABINADados: TBINADados) of object;
  TACBrBinaOnError = procedure(AError: string) of Object;

  TACBrBINA = class(TACBrComponent)
  private
    FAtivo: Boolean;
    FDevice: TACBrDevice;
    FIntervalo: Integer;
    FModelo: TACBrBINAModelo;
    FPorta: String;
    FBINA: IACBrBINA;
    {$IFNDEF NOGUI}
      FTimer: TTimer;
    {$ELSE}
      FTimer: TACBrThreadTimer;
    {$ENDIF}

    FOnLeSerial: TACBrBINALeSerial;
    FOnError: TACBrBinaOnError;
    FSetDataHoraAtivar: Boolean;
    FSetQtdLinhasAoAtivar: Boolean;
    FQtdLinhas: Integer;
    FTempoFlash: Integer;
    function GetPorta: String;
    procedure SetAtivo(const AValue: Boolean);
    procedure SetIntervalo(const AValue: Integer);
    procedure SetModelo(const AValue: TACBrBINAModelo);
    procedure SetPorta(const AValue: String);
    procedure OnTimer(Sender: TObject); virtual;
    procedure SetFQtdLinhas(const AValue: Integer);
    procedure SetFTempoFlash(const AValue: Integer);
    function GetAbout: String;
    procedure SetAbout(const AValue: String);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetDataHora(dia: integer; mes: integer; hora: integer; minuto:integer; segundo:integer); overload;
    procedure SetDataHora(data: TDateTime); overload;
    procedure SetQtdLinhas(linhas: integer; tempoFlash: integer);
    procedure LimparMemoria;
    procedure ObterStatusLinhas;
    procedure ObterProgramacao;
  published
    property AboutACBr : String read GetAbout write SetAbout stored False ;
    property Ativo: Boolean read FAtivo write SetAtivo;
    property Device: TACBrDevice read FDevice;
    property Intervalo: Integer read FIntervalo write SetIntervalo default 1000;
    property Modelo: TACBrBINAModelo read FModelo write SetModelo default binaNenhum;
    property Porta: String read GetPorta write SetPorta;
    property SetDataHoraAtualAoAtivar: Boolean read FSetDataHoraAtivar write FSetDataHoraAtivar default false;
    property SetQtdLinhasAoAtivar: Boolean read FSetQtdLinhasAoAtivar write FSetQtdLinhasAoAtivar default false;
    property OnLeSerial: TACBrBINALeSerial read FOnLeSerial write FOnLeSerial;
    property OnError: TACBrBinaOnError read FOnError write FOnError;
    property QtdLinhas: integer read FQtdLinhas write SetFQtdLinhas default 2;
    property TempoFlash: integer read FTempoFlash write SetFTempoFlash default 3;
  end;

procedure Register;

implementation

uses ACBrUtil, ACBrBINATopTron;

{ TACBrBINA }

constructor TACBrBINA.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FAtivo := False;

  {$IFNDEF NOGUI}
    FTimer := TTimer.Create(self) ;
  {$ELSE}
    FTimer := TACBrThreadTimer.Create ;
  {$ENDIF}

  FTimer.Enabled := False;
  FTimer.OnTimer := OnTimer;

  FDevice := TACBrDevice.Create(Self);
  FDevice.Name := 'ACBrDevice';
  {$IFDEF COMPILER6_UP}
  FDevice.SetSubComponent(True);
  {$ENDIF}
  FDevice.Porta := 'COM1';
  FDevice.TimeOut := 1;

  FQtdLinhas := 2;
  FTempoFlash := 3;
end;

destructor TACBrBINA.Destroy;
begin
  SetAtivo(False);

  FTimer.Free;
  FDevice.Free;

  inherited Destroy;
end;

procedure TACBrBINA.OnTimer(Sender: TObject);
begin
  FTimer.Enabled := False;
  try
    try
      if (FDevice.Ativo) then
      begin
        if (FDevice.BytesParaLer > 0) then
        begin
          FBINA.LeSerial;
          if (Assigned(FOnLeSerial)) then
            FOnLeSerial(FBINA.Dados);
        end;
      end;
    finally
      FTimer.Enabled := True;
    end;
  except
    on e:exception do
    begin
      FTimer.Enabled := False;
      FDevice.Ativo := False;
      FAtivo := False;
      if Assigned(FOnError) then
        FOnError(e.Message);

      raise Exception.Create(e.Message);
    end;
  end;
end;

procedure TACBrBINA.SetAtivo(const AValue: Boolean);
begin
  if (AValue <> FAtivo) then
  begin
    if (AValue) then
      if ((not Assigned(FBINA)) or (FModelo = binaNenhum)) then
      begin
        if Assigned(FOnError) then
          FOnError(ACBrStr('Voc� deve especificar um modelo de BINA!'));

        raise Exception.Create(ACBrStr('Voc� deve especificar um modelo de BINA!'));
      end;

    try
      FDevice.Ativo := AValue;

      if (AValue) then
      begin
        if FSetDataHoraAtivar then
          FBina.SetDataHora(now);

        if FSetQtdLinhasAoAtivar then
          FBina.SetQtdLinhas(FQtdLinhas, FTempoFlash);
      end;

      FTimer.Enabled := AValue;
      FAtivo := AValue;
    except
      on e:exception do
      begin
        if Assigned(FOnError) then
          FOnError(e.Message);

        raise Exception.Create(e.Message);
      end;
    end;
  end;
end;

procedure TACBrBINA.SetIntervalo(const AValue: Integer);
begin
  if (AValue <> FIntervalo) then
  begin
    FTimer.Enabled := False;
    try
      FIntervalo := AValue;
      FTimer.Interval := AValue;
    finally
      FTimer.Enabled := ((FAtivo) and (FIntervalo > 0));
    end;
  end;
end;

procedure TACBrBina.SetFQtdLinhas(const AValue: Integer);
begin
  if (AValue <> FQtdLinhas) then
  begin
    FTimer.Enabled := False;
    try
      FQtdLinhas := AValue;

      if (FAtivo) then
        FBina.SetQtdLinhas(FQtdLinhas, FTempoFlash);

    finally
      FTimer.Enabled := ((FAtivo) and (FQtdLinhas > 0));
    end;
  end;
end;

procedure TACBrBina.SetFTempoFlash(const AValue: Integer);
begin
  if (AValue <> FTempoFlash) then
  begin
    FTimer.Enabled := False;
    try
      FTempoFlash := AValue;

      if (FAtivo) then
        FBina.SetQtdLinhas(FQtdLinhas, FTempoFlash);

    finally
      FTimer.Enabled := ((FAtivo) and (FQtdLinhas > 0)); //Se tem linhas pode ativar, o tempo de flash pode ser 0 (zero)
    end;
  end;
end;

procedure TACBrBINA.SetModelo(const AValue: TACBrBINAModelo);
begin
  if (FModelo <> AValue) then
  begin
    if (FAtivo) then
    begin
      if Assigned(FOnError) then
        FOnError(ACBrStr('N�o � poss�vel mudar o modelo com ACBrBINA ativo!'));

      raise Exception.Create(ACBrStr('N�o � poss�vel mudar o modelo com ACBrBINA ativo!'));
    end;

    FreeAndNil(FBina);

    case (AValue) of
      binaNenhum  : FBINA := nil;
      binaTopTronDD2 : FBINA := TACBrBINATopTronDD2.Create(FDevice);
    end;

    FModelo := AValue;
  end;
end;

function TACBrBINA.GetPorta: String;
begin
  Result := FDevice.Porta;
end;

procedure TACBrBINA.SetPorta(const AValue: String);
begin
  if (FPorta <> AValue) then
  begin
    if (FAtivo) then
    begin
      if Assigned(FOnError) then
        FOnError(ACBrStr('N�o � poss�vel mudar a porta com ACBrBINA ativo!'));

      raise Exception.Create(ACBrStr('N�o � poss�vel mudar a porta com ACBrBINA ativo!'));
    end;

    FDevice.Porta := AValue;
  end;
end;

procedure TACBrBina.SetDataHora(dia: Integer; mes: Integer; hora: Integer; minuto: Integer; segundo: Integer);
begin
  if (FAtivo) then
  begin
    try
      FTimer.Enabled := False;
      FBina.SetDataHora(dia, mes, hora, minuto, segundo);
    finally
      FTimer.Enabled := True;
    end;
  end
  else
  begin
    if Assigned(FOnErroR) then
      FOnError(ACBrStr('N�o � poss�vel setar a data/hora sem ativar o ACBrBINA'));

    raise Exception.Create(ACBrStr('N�o � poss�vel setar a data/hora sem ativar o ACBrBINA'));
  end;
end;

procedure TACBrBina.SetDataHora(data: TDateTime);
begin
  if (FAtivo) then
  begin
    try
      FTimer.Enabled := False;
      FBina.SetDataHora(data);
    finally
      FTimer.Enabled := True;
    end;
  end
  else
  begin
    if Assigned(FOnError) then
      FOnError(ACBrStr('N�o � poss�vel setar a data/hora sem ativar o ACBrBINA'));

    raise Exception.Create(ACBrStr('N�o � poss�vel setar a data/hora sem ativar o ACBrBINA'));
  end;
end;

procedure TACBrBina.LimparMemoria;
begin
  if (FAtivo) then
  begin
    try
      FTimer.Enabled := False;
      FBina.LimparMemoria;
    finally
      FTimer.Enabled := True;
    end;
  end
  else
  begin
    if Assigned(FOnError) then
      FOnError(ACBrStr('N�o � poss�vel limpar a mem�ria sem ativar o ACBrBINA'));

    raise Exception.Create(ACBrStr('N�o � poss�vel limpar a mem�ria sem ativar o ACBrBINA'));
  end;
end;

procedure TACBrBina.ObterStatusLinhas;
begin
  if (FAtivo) then //Para obter o status das linhas n�o � necess�rio para o timer (ao menos no toptron(unico modelo disponivel at� o momento))
    FBina.ObterStatusLinhas;
end;

procedure TACBrBina.ObterProgramacao;
begin
  if (FAtivo) then   //Para obter a programa��o n�o � necess�rio para o timer (ao menos no toptron(unico modelo disponivel at� o momento))
    FBina.ObterProgramacao;
end;

procedure TACBrBina.SetQtdLinhas(linhas: integer; tempoFlash: integer);
begin
  if (FAtivo) then
  begin
    try
      FTimer.Enabled := False;
      FBina.SetQtdLinhas(linhas, tempoFlash);
      FQtdLinhas := linhas;
      FTempoFlash := tempoFlash;
    finally
      FTimer.Enabled := True;
    end;
  end
  else
  begin
    if Assigned(FOnError) then
      FOnError(ACBrStr('N�o � poss�vel configurar as linhas/tempo de Flash sem ativar o ACBrBINA'));

    raise Exception.Create(ACBrStr('N�o � poss�vel configurar as linhas/tempo de Flash sem ativar o ACBrBINA'));
  end;
end;

function TACBrBina.GetAbout: String;
begin
   Result := 'ACBrBINA Ver: ' + CACBrBINA_Versao;
end;

procedure TACBrBina.SetAbout(const AValue: String);
begin
   {}
end;


procedure Register;
begin
  RegisterComponents('ACBrSerial', [TACBrBINA]);
end;


end.
