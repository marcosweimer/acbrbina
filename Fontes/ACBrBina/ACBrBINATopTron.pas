{******************************************************************************}
{ Projeto: Componentes ACBr                                                    }
{  Biblioteca multiplataforma de componentes Delphi para intera��o com equipa- }
{ mentos de Automa��o Comercial utilizados no Brasil                           }
{                                                                              }
{ Direitos Autorais Reservados (c) 2004 Daniel Simoes de Almeida               }
{                                                                              }
{ Colaboradores nesse arquivo:                                                 }
{                                                                              }
{  Voc� pode obter a �ltima vers�o desse arquivo na pagina do  Projeto ACBr    }
{ Componentes localizado em      http://www.sourceforge.net/projects/acbr      }
{                                                                              }
{ Esse arquivo usa a classe  SynaSer   Copyright (c)2001-2003, Lukas Gebauer   }
{  Project : Ararat Synapse     (Found at URL: http://www.ararat.cz/synapse/)  }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{                                                                              }
{ Daniel Sim�es de Almeida  -  daniel@djsystem.com.br  -  www.djsystem.com.br  }
{              Pra�a Anita Costa, 34 - Tatu� - SP - 18270-410                  }
{                                                                              }
{******************************************************************************}

{******************************************************************************
|* Historico
|*
|* 29/02/2016: Marcos R. Weimer
|*  - Primeira Versao ACBrBINATopTron
******************************************************************************}

{$I ACBr.inc}

unit ACBrBINATopTron;

interface

uses
  Classes, SysUtils,
  ACBrDevice,
  ACBrBINAInterface;

type
  TACBrBINATopTronDD2 = class(TInterfacedObject, IACBrBINA)
  private
    FDados: TBINADados;
    FDevice: TACBrDevice;
    function GetBINADados: TBINADados;
    procedure EnviaByte(strEnv: string; intervalo: integer = 5);
  public
    constructor Create(ADevice: TACBrDevice);
    destructor Destroy; override;
    procedure LeSerial;
    procedure SetDataHora(dia: integer; mes: integer; hora: integer; minuto:integer; segundo:integer); overload;
    procedure SetDataHora(data: TDateTime); overload;
    procedure SetQtdLinhas(linhas: integer; tempoFlash: integer);
    procedure LimparMemoria;
    procedure ObterStatusLinhas;
    procedure ObterProgramacao;
    property Dados: TBINADados read GetBINADados;
  end;

implementation

{ TACBrBINATopTron }

constructor TACBrBINATopTronDD2.Create(ADevice: TACBrDevice);
begin
  inherited Create;

  FDevice       := ADevice;
  FDevice.Baud  := 19200;
  FDevice.Data  := 8;
  FDevice.Parity:= pNone;
  FDevice.Stop  := s1;
end;

destructor TACBrBINATopTronDD2.Destroy;
begin
  inherited Destroy;
end;

procedure TACBrBINATopTronDD2.LeSerial;
  function HexToBinL(Hexadecimal: string): string;
  const
    BCD: array [0..15] of string =
      ('0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111',
      '1000', '1001', '1010', '1011', '1100', '1101', '1110', '1111');
  var
    i: integer;
  begin
    for i := Length(Hexadecimal) downto 1 do
      Result := BCD[StrToInt('$' + Hexadecimal[i])] + Result;
  end;

var
  S: String;
  i: integer;
begin
  FDados := Default(TBinaDados);
  if (FDevice.Ativo) then
  begin
    if (FDevice.BytesParaLer > 0) then
    begin
      try
        FDados.Comando := FDevice.LeString();
        FDados.ComandoTipo := tcNenhum;

        S := Copy(FDados.Comando, 1, 1);

        if S.Equals('G') then //Status das linhas
        begin
          {$region 'Status das linhas'}

          {
             Sequ�ncia enviada quando uma chamada for completada ou finalizada (Tarifa��o): (G) (x1) (x2)

             (G): Inicio do Telegrama
             (x1): (0 a E)   N�mero do aparelho ou do sistema.
             (x2): (0 a F) (0000 a 1111 em bin�rio) Representa��o Bit a Bit  do status de gancho linha a linha.
                   x3  x2  x1 x0
                   x3 = linha 4,
                   x2 = linha 3,
                   x1 = linha 2,
                   x0 = linha 1,

             Bit = 0 (fone no gancho)
             Bit = 1  (fora do gancho ou inexistente)

             EX: Atendimento de uma chamada na linha 2 com linha 1, 3 , 4 no gancho) e sistema 0.
                 A string enviada ser�  �G02� correspondente a  G0(0010b)
          }

          S := FDados.Comando;

          FDados.ComandoTipo := tcStatusLinha;
          FDados.NumeroAparelho := Copy(S, 2, 1);

          S := HexToBinL(Copy(S, 3, 1));

          SetLength(FDados.Linhas, Length(S)); //Seta a quantidade de linhas

          for i := 0 to Length(S) - 1 do
          begin
            FDados.Linhas[i] := TStatusLinha(StrToInt(copy(S, i, 1)));
          end;

          {$endregion}
        end
        else if S.Equals('H') then //Programa��o do aparelho
        begin
          {$region 'Programa��o'}

          {
            O aparelho envia, ao computador, uma string referente aos par�metros programados na
            mem�ria interna: (H) (y1,y2) (y3,y4) (y5,y6) (y7,y8) (y9,y10 (y11,y12) (VrXYZW)@@@

            (H): In�cio da String
            (y1,y2): (de 01h a 1Fh) Dia
            (y3,y4): (de 00h a 0Ch) M�s
            (y5,y6): (de 00h a 17h) Horas
            (y7,y8): (de 00h a 3Bh) Minutos
            (y9,y10): (de 00h a 3Bh) Segundos
            (y11,y12): (de 01h a 28h) N�mero de Linhas (01h corresponde a 1 linha, e assim por diande)
            (y13,y14): (de 00h a 09h) Tempo de Flash (00h corresponde ao tempo de 0, e assim por diante)
            (VrXYZW):  Vers�o do Microcontrolador (Por Ex: Vr23TC)
            @@@:   Final de String

            OBS: Na string enviada, a parte correspondente ao Dia / M�s / Horas / Minutos do rel�gio
                 interno do aparelho ser�o representadas no formato hexadecimal.
                 Assim,  11d de 05d (Maio) corresponde a 0Bh de 05h e 22d Horas e 12d Min.corresponde
                 a 16h Horas e 0Ch Min

            Exemplo: H00FF0005342800Vr2vFK@@@
          }

          S := FDados.Comando;

          FDados.ComandoTipo := tcProgramacao;

          S := copy(S, 2, Length(S) - 4); //remove inicio/fim da string

          //Dia
          FDados.Configuracao.Dia := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove o dia da string

          //M�s
          FDados.Configuracao.Mes := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove o m�s da string

          //Hora
          FDados.Configuracao.Hora := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove a hora da string

          //Minutos
          FDados.Configuracao.Minutos := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove os minutos

          //Segundos
          FDados.Configuracao.Segundos := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove os segundos

          //N�mero de Linhas
          FDados.Configuracao.NumeroLinhas := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove n�mero de linhas

          //Tempo de Flash
          FDados.Configuracao.TempoFlash := StrToInt('$' + Copy(S, 1, 2));
          S := Copy(S, 3, Length(S) - 2); //remove tempo de Flash

          //Vers�o do BINA
          FDados.Configuracao.Versao := S; //oque sobra da string � a vers�o

          {$endregion}
        end
        else if ((S.Equals('A')) or (S.Equals('B'))) then
        begin
          {$region 'Chamadas Recebidas/Efetuadas'}

          {
             DADOS SER�O ENVIADOS EM C�DIGO ASCII AO COMPUTADOR
             (A/B) (x1) (x2) (x3) (x4, x5, x6,....) (E)  -  comprimento vari�vel

             (A/B): A = Chamada Recebida B = Chamada Efetuada
             (x1): (0 a E) Numero do aparelho (eles podem ser ligados em cascata)
             (x2): (1 a 8) N�mero da linha telef�nica.
             (x3): (0 a 9) Para chamadas recebidas significa a categoria do assinante.
                                Para chamadas efetuadas significo primeiro numero discado.
             (x4,x5,...): (0 a 9) N�meros do telefone das chamadas.
             (E): Final de Telegrama

             Exemplo:
             Chamada Recebida do numero 04832476868, na linha 3 do primeiro aparelho: �A0314832476868E�

             Observa��es:
             - Os programas dever�o reconhecer o ASCII �A� ou �B� e contar o n�mero de entradas at� o
               final da string ou seja, at� receber o ASCII �E�.
             - O n�mero telef�nico deve ser contado a partir do �ltimo n�mero recebido para o primeiro.
             - Assim, os 8 (ou 7) �ltimos n�meros correspondem ao n�mero telef�nico, os 2 anteriores
               ao c�digo de �rea, e o imediatamente anterior ao c�digo de �rea, corresponde a categoria
               do telefone chamador.

             Exemplo:
             String recebida �A1214832473550E�
             N�mero telef�nico = 32473550
             C�digo de �rea = 48
             categoria = 1
             linha 2
             sistema 1 = 11 (6a. linha)
             Trata-se de uma chamada recebida = A
          }

          S := FDados.Comando;

          FDados.ComandoTipo := tcChamada;

          if (Copy(S, 1, 1) = 'A') then
            FDados.Chamadas.TipoChamada := tcRecebida
          else
            FDados.Chamadas.TipoChamada := tcEfetuada;

          FDados.NumeroAparelho := Copy(S, 2, 1);
          FDados.Chamadas.NumeroLinha := Copy(S, 3, 1);

          if FDados.Chamadas.TipoChamada = tcRecebida then
          begin
            FDados.Chamadas.CategoriaAssinante := TCategoriaAssinante(StrToInt(Copy(S, 4, 1)));
            FDados.Chamadas.Telefone := Copy(S, 5, Pos('E', S) -5);
          end
          else
          begin
            FDados.Chamadas.CategoriaAssinante := caNenhum;
            FDados.Chamadas.Telefone := Copy(S, 4, Pos('E', S) -4);
          end;

          //FDados.Chamadas.Telefone := Copy(S, 4, Pos('E', S) -5);



          {$endregion}
        end;
      except
        on E:Exception do
          FDados := Default(TBINADados);
      end;
    end;
  end;
end;

function TACBrBINATopTronDD2.GetBINADados: TBINADados;
begin
  Result := FDados;
end;

procedure TACBrBINATopTronDD2.SetDataHora(dia: integer; mes: integer; hora: integer; minuto:integer; segundo:integer);
begin
  {
    Acertar Data e Rel�gio do Identificador
    Comando �f (ASCII 66h) + (y1,y2) + (y3,y4) + (y5,y6) + (y7,y8) + (y9,y10) �
  }

  if (FDevice.Ativo) then
  begin
    FDevice.EnviaString('f');
    Sleep(5);
    EnviaByte(IntToHex(dia, 2));
    EnviaByte(IntToHex(mes, 2));
    EnviaByte(IntToHex(hora, 2));
    EnviaByte(IntToHex(minuto, 2));
    EnviaByte(IntToHex(segundo, 2));
  end;
end;

procedure TACBrBINATopTronDD2.SetDataHora(data: TDateTime);
var
  dia, mes, ano: Word;
  hora, minuto, segundo, milesegundo: Word;
begin
   DecodeDate(data, ano, mes, dia);
   DecodeTime(data, hora, minuto, segundo, milesegundo);

   SetDataHora(dia, mes, hora, minuto, segundo);
end;

procedure TACBrBINATopTronDD2.LimparMemoria;
begin
  {
    Apaga e Limpa a mem�ria interna do identificador referente as chamadas armazenadas
    Comando �d� (ASCII 64h)
  }

  if (FDevice.Ativo) then
    FDevice.EnviaString('d');
end;

procedure TACBrBinaTopTronDD2.ObterStatusLinhas;
begin
  {
     Obter em tempo real o Status das Linhas telef�nicas (Gancho/Inexistente)
     Comando �h� (ASCII 68h)
  }

  if (FDevice.Ativo) then
    FDevice.EnviaString('h');
end;

procedure TACBrBinaTopTronDD2.ObterProgramacao;
begin
  {
    Obter dados da programa��o armazenada no aparelho conectado ao computador
    Comando �l� (ASCII 6Ch) (Sem efeito para o CALLER ID T4)
  }

  if (FDevice.Ativo) then
    FDevice.EnviaString('l');
end;

procedure TACBrBINATopTronDD2.SetQtdLinhas(linhas: integer; tempoFlash: integer);
begin
  {
    Programa o aparelho com o n�mero de linhas e tempo de flash
    Comando �j� (ASCII 6Ah) + (y1,y2) + (y3,y4) (Sem efeito para o CALLER ID T4)
  }

  if (FDevice.Ativo) then
  begin
    FDevice.EnviaString('j');
    Sleep(5);
    EnviaByte(IntToHex(linhas, 2));
    EnviaByte(IntToHex(tempoFlash, 2));
  end;
end;

procedure TACBrBINATopTronDD2.EnviaByte(strEnv: string; intervalo: integer = 5);
var
  bytes: tbytes;
begin
  SetLength(Bytes, Length(strEnv) div 2);
  HexToBin(PChar(strEnv), Bytes[0], Length(Bytes));
  FDevice.EnviaByte(bytes[0]);
  Sleep(intervalo);
end;


end.
