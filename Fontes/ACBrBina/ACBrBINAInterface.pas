{******************************************************************************}
{ Projeto: Componentes ACBr                                                    }
{  Biblioteca multiplataforma de componentes Delphi para intera��o com equipa- }
{ mentos de Automa��o Comercial utilizados no Brasil                           }
{                                                                              }
{ Direitos Autorais Reservados (c) 2004 Daniel Simoes de Almeida               }
{                                                                              }
{ Colaboradores nesse arquivo:                                                 }
{                                                                              }
{  Voc� pode obter a �ltima vers�o desse arquivo na pagina do  Projeto ACBr    }
{ Componentes localizado em      http://www.sourceforge.net/projects/acbr      }
{                                                                              }
{ Esse arquivo usa a classe  SynaSer   Copyright (c)2001-2003, Lukas Gebauer   }
{  Project : Ararat Synapse     (Found at URL: http://www.ararat.cz/synapse/)  }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{                                                                              }
{ Daniel Sim�es de Almeida  -  daniel@djsystem.com.br  -  www.djsystem.com.br  }
{              Pra�a Anita Costa, 34 - Tatu� - SP - 18270-410                  }
{                                                                              }
{******************************************************************************}

{******************************************************************************
|* Historico
|*
|* 29/02/2016: Marcos R. Weimer
|*  - Primeira Versao ACBrBINAInterface
******************************************************************************}

{$I ACBr.inc}

unit ACBrBINAInterface;

interface

uses ACBrDevice;

type
  TTipoChamada = (tcEfetuada, tcRecebida);
  TStatusLinha = (slLivre, slOcupada);
  TTipoComando = (tcNenhum, tcStatusLinha, tcProgramacao, tcChamada);

  {
    Categoria do Assinante:
    1- Assinante normal (residencial, comercial ou celular)
    2- Assinante normal (celular)
    3- Companhia telef�nica
    4- Telefone p�blico
    5- Operador da companhia telef�nica
    6- Linha de dados
    7- Telefone p�blico interurbano ou cart�o
  }
  TCategoriaAssinante = (caNenhum, caResidencialComercialCelular, caCelular, caCompanhiaTelefonica, caTelefonePublico, caOperadorCompanhiaTelefonica, caLinhaDados, caTelefonePublicoInterurbanoOuCartao);

 { TBINADados = record
    TipoChamada   : TTipoChamada;
    NumeroAparelho: String[1];
    NumeroLinha   : String[1];
    Telefone      : String;
    Linhas        : array of TStatusLinha;
  end;
  }

  TBINAConfig = record
    Dia: Integer;
    Mes: Integer;
    Hora: Integer;
    Minutos:Integer;
    Segundos: Integer;
    NumeroLinhas: Integer;
    TempoFlash: Integer;
    Versao: string;
  end;

  TBINAChamadas = record
    TipoChamada: TTipoChamada;
    NumeroLinha: String[1];
    CategoriaAssinante: TCategoriaAssinante;
    Telefone: String;
    Dia: Integer;
    Mes: Integer;
    Hora_Inicio: Integer;
    Minuto_Inicio: Integer;
    Hora_Conversa: Integer;
    Minuto_Conversa: Integer;
    Segundo_Conversa: Integer;
  end;

  TBINADados = record
    Comando: string;
    ComandoTipo: TTipoComando;
    NumeroAparelho: String[1];
    Linhas: array of TStatusLinha;
    Configuracao: TBinaConfig;
    Chamadas: TBINAChamadas;
  end;

  IACBrBINA = interface
    ['{7342E530-D5FF-4B5D-AC76-26967A948A40}']
    //constructor Create(ADevice: TACBrDevice);
    procedure LeSerial;
    procedure SetDataHora(dia: integer; mes: integer; hora: integer; minuto:integer; segundo:integer); overload;
    procedure SetDataHora(data: TDateTime); overload;
    procedure SetQtdLinhas(linhas: integer; tempoFlash: integer);
    procedure LimparMemoria;
    procedure ObterStatusLinhas;
    procedure ObterProgramacao;
    function GetBINADados: TBINADados;
    property Dados: TBINADados read GetBINADados;
  end;

implementation

end.
