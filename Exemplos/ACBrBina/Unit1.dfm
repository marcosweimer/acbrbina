object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Teste BINA TopTron DD2 USB'
  ClientHeight = 441
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 328
    Height = 441
    Align = alClient
    Caption = '[ ACBrBina ]'
    TabOrder = 0
    object mLog: TMemo
      Left = 2
      Top = 106
      Width = 324
      Height = 333
      Align = alClient
      Lines.Strings = (
        'Memo2')
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 324
      Height = 91
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 184
        Top = 8
        Width = 30
        Height = 13
        Caption = 'Porta:'
      end
      object btnAtivar: TButton
        Left = 8
        Top = 0
        Width = 75
        Height = 25
        Caption = 'Iniciar'
        TabOrder = 0
        OnClick = btnAtivarClick
      end
      object btnParar: TButton
        Left = 89
        Top = 0
        Width = 75
        Height = 25
        Caption = 'Parar'
        TabOrder = 1
        OnClick = btnPararClick
      end
      object cbPorta: TComboBox
        Left = 226
        Top = 3
        Width = 89
        Height = 21
        ItemIndex = 3
        TabOrder = 2
        Text = 'COM4'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8'
          'COM9')
      end
      object btnEnvDateTime: TButton
        Left = 8
        Top = 30
        Width = 156
        Height = 25
        Caption = 'Enviar data/hora'
        TabOrder = 3
        OnClick = btnEnvDateTimeClick
      end
      object btnGetStatusLinhas: TButton
        Left = 170
        Top = 30
        Width = 145
        Height = 25
        Caption = 'Obter Status das Linhas'
        TabOrder = 4
        OnClick = btnGetStatusLinhasClick
      end
      object btnLimparMemoria: TButton
        Left = 8
        Top = 57
        Width = 157
        Height = 25
        Caption = 'Limpar memoria'
        TabOrder = 5
        OnClick = btnLimparMemoriaClick
      end
      object btnObterProg: TButton
        Left = 170
        Top = 57
        Width = 145
        Height = 25
        Caption = 'Obter programa'#231#227'o'
        TabOrder = 6
        OnClick = btnObterProgClick
      end
    end
  end
  object ACBrBINA1: TACBrBINA
    Ativo = False
    Intervalo = 5
    Modelo = binaTopTronDD2
    Porta = 'COM4'
    SetDataHoraAtualAoAtivar = True
    SetQtdLinhasAoAtivar = True
    OnLeSerial = ACBrBINA1LeSerial
    TempoFlash = 1
    Left = 200
    Top = 168
  end
end
