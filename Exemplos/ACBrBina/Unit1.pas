﻿unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ACBrDevice, Vcl.StdCtrls, Vcl.ExtCtrls, System.StrUtils,
  ACBrBase, ACBrBINA, ACBrBinaInterface;

type
  TForm1 = class(TForm)
    ACBrBINA1: TACBrBINA;
    GroupBox1: TGroupBox;
    mLog: TMemo;
    Panel1: TPanel;
    btnAtivar: TButton;
    btnParar: TButton;
    Label1: TLabel;
    cbPorta: TComboBox;
    btnEnvDateTime: TButton;
    btnGetStatusLinhas: TButton;
    btnLimparMemoria: TButton;
    btnObterProg: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnAtivarClick(Sender: TObject);
    procedure btnPararClick(Sender: TObject);
    procedure btnEnvDateTimeClick(Sender: TObject);
    procedure btnLimparMemoriaClick(Sender: TObject);
    procedure ACBrBINA1LeSerial(ABINADados: TBINADados);
    procedure btnGetStatusLinhasClick(Sender: TObject);
    procedure btnObterProgClick(Sender: TObject);
  private
    procedure AtivarDesativarComponentes;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.ACBrBINA1LeSerial(ABINADados: TBINADados);
var
  i: integer;
begin
  mLog.Lines.Add('--- ' + DateTimeToStr(now) + ' ---');
  mLog.Lines.Add('Comando: ' + ABINADados.comando);

  case ABinaDados.ComandoTipo of
    tcNenhum:
      begin
        mLog.Lines.Add('Tipo: Desconhecido');
      end;
    tcStatusLinha:
      begin
         mLog.Lines.Add('Tipo: Status Linha');
         for i := 0 to High(ABinaDados.Linhas) do
           mLog.Lines.Add('Linha ' + IntToStr(i) + ': ' + ifThen(ABinaDados.Linhas[i] = slLivre, 'Livre', 'Ocupada/Inexistente'));
      end;
    tcProgramacao:
      begin
        mLog.Lines.Add('Tipo: Programação');
        mLog.Lines.Add('Dia: ' + IntToStr(ABinaDados.Configuracao.Dia));
        mLog.Lines.Add('Mês: ' + IntToStr(ABinaDados.Configuracao.Mes));
        mLog.Lines.Add('Horas: ' + IntToStr(ABinaDados.Configuracao.Hora));
        mLog.Lines.Add('Minutos: ' + IntToStr(ABinaDados.Configuracao.Minutos));
        mLog.Lines.Add('Segundos: ' + IntToStr(ABinaDados.Configuracao.Segundos));
        mLog.Lines.Add('Número de linhas: ' + IntToStr(ABinaDados.Configuracao.NumeroLinhas));
        mLog.Lines.Add('Tempo de flash: ' + IntToStr(ABinaDados.Configuracao.TempoFlash));
        mLog.Lines.Add('Versão do BINA: ' + ABinaDados.Configuracao.Versao);
      end;
    tcChamada:
      begin
        mLog.Lines.Add('Tipo: Chamada');
        mLog.Lines.Add('Aparelho: ' + ABinaDados.NumeroAparelho);
        mLog.Lines.Add('Num. Linha: ' + ABinaDados.Chamadas.NumeroLinha);

        case ABinaDados.Chamadas.CategoriaAssinante of
          caNenhum: mLog.Lines.Add('Categoria assinante: Não identificada');
          caResidencialComercialCelular: mLog.Lines.Add('Categoria assinante: 1 - Assinante normal (residencial, comercial ou celular)');
          caCelular: mLog.Lines.Add('Categoria assinante: 2- Assinante normal (celular)');
          caCompanhiaTelefonica: mLog.Lines.Add('Categoria assinante: 3- Companhia telefônica');
          caTelefonePublico: mLog.Lines.Add('Categoria assinante: 4- Telefone público');
          caOperadorCompanhiaTelefonica: mLog.Lines.Add('Categoria assinante: 5- Operador da companhia telefônica');
          caLinhaDados: mLog.Lines.Add('Categoria assinante: 6- Linha de dados');
          caTelefonePublicoInterurbanoOuCartao: mLog.Lines.Add('Categoria assinante: 7- Telefone público interurbano ou cartão');
        end;

        mLog.Lines.Add('Telefone: ' + ABinaDados.Chamadas.Telefone);
        mLog.Lines.Add('Dia: ' + IntToStr(ABinaDados.Chamadas.Dia));
        mLog.Lines.Add('Mês: ' + IntToStr(ABinaDados.Chamadas.Mes));
        mLog.Lines.Add('Hora Inicio: ' + IntToStr(ABinaDados.Chamadas.Hora_Inicio));
        mLog.Lines.Add('Minuto Inicio: ' + IntToStr(ABinaDados.Chamadas.Minuto_Inicio));
        mLog.Lines.Add('Hora Conversa: ' + IntToStr(ABinaDados.Chamadas.Hora_Conversa));
        mLog.Lines.Add('Minuto Conversa: ' + IntToStr(ABinaDados.Chamadas.Minuto_Conversa));
        mLog.Lines.Add('Segundos Conversa: ' + IntToStr(ABinaDados.Chamadas.Segundo_Conversa));
      end;
  end;

end;

procedure TForm1.btnPararClick(Sender: TObject);
begin
  ACBrBINA1.Ativo := False;

  if ACBrBINA1.Ativo then
    mLog.Lines.Add('Não foi possível desativar')
  else
    mLog.Lines.Add('Desativado');

  AtivarDesativarComponentes;
end;

procedure TForm1.btnEnvDateTimeClick(Sender: TObject);
begin
  mLog.Lines.Add('Enviando data 16/02 17:10:00');
  ACBrBINA1.SetDataHora(16,2,17,10,00);
  mLog.Lines.Add('Enviado');
end;

procedure TForm1.btnObterProgClick(Sender: TObject);
begin
  mLog.Lines.Add('Obtendo programação (ObterProgramacao)');
  ACBrBina1.ObterProgramacao;
end;

procedure TForm1.btnLimparMemoriaClick(Sender: TObject);
begin
  mLog.Lines.Add('Limpar memória');
  ACBrBINA1.LimparMemoria;
  mLog.Lines.Add('Limpa');
end;

procedure TForm1.btnGetStatusLinhasClick(Sender: TObject);
begin
  mLog.Lines.Add('Obtendo status das linhas (ObterStatusLinhas)');
  ACBrBINA1.ObterStatusLinhas;
end;

procedure TForm1.btnAtivarClick(Sender: TObject);
begin
  ACBrBINA1.Porta := cbPorta.items[cbPorta.ItemIndex];
  ACBrBINA1.Ativo := True;

  if (ACBrBINA1.Ativo) then
    mLog.Lines.Add('Ativado')
  else
    mLog.Lines.Add('Não foi possível ativar');

  AtivarDesativarComponentes;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mLog.Lines.Clear;
  AtivarDesativarComponentes;
end;

procedure TForm1.AtivarDesativarComponentes;
var
  ativo: boolean;
begin
  ativo := ACBrBINA1.Ativo;

  btnAtivar.Enabled := (not ativo);
  cbPorta.Enabled := (not ativo);

  btnParar.Enabled := ativo;
  btnEnvDateTime.Enabled := ativo;
  btnGetStatusLinhas.Enabled := ativo;
  btnLimparMemoria.Enabled := ativo;
  btnObterProg.Enabled := ativo;

end;

end.
