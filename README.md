Ola!

Este é um repositório temporário para um novo componente ACBrBina.

Toda colaboração é bem vinda!

O objetivo principal neste momento é deixar o componente nos padrões do ACBr para que possa ser integrado ao mesmo.

Pendências para integrar no repositório oficial:

- Falta o cabeçalho do ACBr nos fontes (Feito)

- Falta chamar ACBr.inc no inicio dos fontes (Feito)

- Faltam diretivas de compilação para modo CONSOLE... não temos TTimer em modo CONSOLE...  (veja como foi implementado em ACBrLCB.pas) (Feito)

- O método "SetModelo", parece errado, e pode causar MemoryLeak (ele não libera o modelo anterior) (Feito)

- Seria melhor ter um método para o disparo de Exceptions... algo como "DoException"... e dentro desse método, você pode chamar "FOnError", isso economizaria várias linhas de código... (Pendente)

- Em "OnTimer", se ocorrer um Exception, o monitoramento irá parar... Isso é intencional ? R: Sim, é intencional, qual o melhor modo de tratar? (Pendente)

- É mais indicado usar o nome do Protocolo do que do equipamento (TopTron)... (ou pelo menos o nome do Equipamento que foi o primeiro a introduzir este protocolo) (Pendente)


**Post no forum do ACBr:** 

http://www.projetoacbr.com.br/forum/topic/11938-integra%C3%A7%C3%A3o-com-bina/

**Equipamentos suportados:**

- TopTron DD2 USB (testado por marcosweimer no desenvolvimento do componente)

- TopTron DD4 USB (testado por joaovmf conforme post: http://www.projetoacbr.com.br/forum/topic/11938-integra%C3%A7%C3%A3o-com-bina/?page=2#comment-224299)